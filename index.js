/*
	db.users.insert({
	    firstName: "Jane",
	    lastName: "Doe",
	    age: 21,
	    contact: {
	        phone: "87654321",
	        email: "janedoe@gmail.com"
	    },
	    courses: [ "CSS", "Javascript", "Python" ],
	    department: "none"
	});


*/

// Advance Query Operators
// [SECTION] Comparison Query Operators

// $gt/$gte operator 
	 /*
		- Allows us to find documents that have field number values greater than or equal to specified value.
		- Syntax:
			db.collectionName.find({field:{$gt/$gte: value}});
	 */

db.users.find(
	{
		age:{
			$gt: 65 //greater than (>)
		}
	}
);

db.users.find(
	{
		age:{
			$gte: 65 //greater than or equal (>=)
		}
	}
);

//---------------------------------

	//$lt/$lte operator
	/*
		- Allows us to find documents that have the field number values less than or equal to a specified value.
		- Syntax:
			db.collectionName.find({field: {$lt/$lte: value}});
	*/
	
	db.users.find(
		{
			age:{
				$lt: 65 //lesser than (<)
			}
		}
	);

	db.users.find(
		{
			age:{
				$lte: 65 //lesser than or equal (=<)
			}
		}
	);

	// $ne operator
	/*
			- Allows us to find documents field number values not equal to a specified value.
			-Syntax:
				db.collectionName.find({field: { $ne: value}});
	*/

	db.users.find(
		{
			age:{
				$ne: 82 //'ne' means not equal
			}
		}
	);

	// $in operator
	/*
		- Allows us to find documents with specific match criteria one with one field using different values.
        -  Selects the documents where the value of a field equals any value in the specified array. 
		- Syntax:
			db.collectionName.find({field: {$in: [valueA, valueB]}});
	*/

	db.users.find(
		{
			lastName:{
				$in: ['Hawking', 'Doe']
			}

		}
	);


	db.users.find(
		{
			courses:{
				$in: ['HTML', 'React']
			}

		}
	);


	// [SECTION] Logical Query Operators

	// $or operator
	/*
		- Allows us to find documents that match a single criteria from multiple provide search criteria
		- Syntax:
			db.collectionName.find({ $or: [{fieldA:valueA}, {fieldB:valueB}]});
	*/

	// Multiple field value pairs
	db.users.find(
		{
			$or: [
				{firstName: "Neil"},
				{age : 25}
			]
		}
	)

	db.users.find(
		{
			$or: [
				{firstName: "Neil"},
				{age : {$gt: 25}}
			]
		}
	)

// $and operator 
/*
	- Allows us to find documents matching multiple criteria in a single field.
	- Syntax:
		db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}]});
*/

db.users.find(
		{
		$and: [ {age: {$ne: 82}},
				{age: {$ne: 76}}
			]

	}

);


db.users.find(
		{
		$and: [ {age: {$ne: 82}},
				{department : "Operations"}
			]

	}

);

 //$or vs $and

/*

	$or - even just '1' is true the value will be "true"
	$and - 'all' must be true
	
	$or 
	t + t = t 
	t + f = t 
	f + t = t 
	f + f = f 
    t + f + f + f + f  = t 

    $and 
    t + t = t 
    t + f  = f 
    f + t = f 
    f + f  = f 
    t + t + t + f + t = f 

*/


// [SECTION] Field Projection
// To help with the readability of the values returned, we can include/exclude fields from the response.

// Inclusion (1)
/*
	- Allows us to include/add specific fields only when retrieving a document.
	- The value provided is 1 to denote that the field is being included in the retrieval.
	- Syntax:
		db.collectionName.find({criteria}, {field: 1});
*/

//1 = fields you want to include. (true)
//0 = fields you watn to exclude. (false)

// first line is what / who you want to search. 
// This will only show the "first name, last name, and contact."
 

 db.users.find(
 		{
 			firstName: "Jane"
 		},
 		{
 			firstName: 1,
 			lastName: 1,
 			contact: 1
 		}
 	);


// Exclusion (2)
/*
	- Allows us to exclude/remove specific fields only when retrieving documents.
	- The value provided is 0 to denote that fields is being excluded.
	- Syntax:
	 	db.users.find({criteria}, {field: 0});
*/


 db.users.find(
 	{
 		firstName : "Jane"
 	},
 	{
 		contact: 0,
 		department: 0,
 	}

 	)

//excludes contact and department from the view. 

// Supressing the ID field
	/*
		- When using field projection, field "inclusion" and "exclusion" may not be used at the same time.
		- Excluding the "_id" field is the only exception to this rule.
		- Syntax: 
			db.collectionName.find({critea}, {_id:0})

	*/

 db.users.find(
 	{
 		firstName : "Jane"
 	},

 	{
 		 firstName: 1,
 		 lastName: 1,
 		 contact: 1, 
 	},
 	{
 		 department: 0, // No need 
 	}

 	);


  db.users.find(
 	{
 		firstName : "Jane"
 	},

 	{
 		 firstName: 1,
 		 lastName: 1,
 		 _id: 0 // can only be excluded. 
 	}

 	);

  // --------------------------

  // [SECTION] Evaluation Query Operators
  // $regex operator
  /*
	- Allows us to find documents that match a specific string pattern using regular expressions.
	- Syntax:
		db.users.find({field: $regex: "pattern", $options: $optionValue});
  */

  db.users.find(
 	 {
 	 	firstName : {
 	 		$regex: 'N' // also case sensitive 
 	 	}
 	 }

  	);

  db.users.find(
		{
			$or: [
				{firstName: {$regex:'a',}},
				{lastName: {$regex:'A',}}
			]
		}
	)

    db.users.find(
		{
			$or: [
				{firstName: {$regex:'a',}},
				{lastName: {$regex:'A',}},
			]
		},
		{ firstName : 1,
		  lastName: 1,
		  _id:0
		}
	);

    db.users.find(


    db.users.find(
 		{
 			firstName: "Jane"
 		},
 		{
 			firstName: 1,
 			lastName: 1,
 			contact: 1
 		}
 	);
